#!/python3
"""
A demonstration of reading, parsing, and acting on a stream using Python.

For compactness, it also implements a stream writer using a pseudo tty.
"""
import io
import logging
import os
import pty
import select
import threading
import time
import serial

from collections import deque
from itertools import cycle


# Simple logging
log = logging
log.basicConfig(level=logging.DEBUG)



class SimpleStream:
    """Class for opening a pseudo tty and writing repeating data to it for use
    as a proof-of-concept when real streams are unavailable/inconvenient.
    """
    def __init__(self, interval=1):
        self.interval = interval

        # Create a pseudo terminal
        self.master, self.slave = pty.openpty()
        self.slave_name = os.ttyname(self.slave)

        log.info("Created pty: %s"%self.slave_name)

        # The generator for creating data
        self.gen = cycle((i.to_bytes(1, 'big') for i in range(256)))

    def run(self):
        log.info("Started streaming to: %s"%self.slave_name)
        while True:
            data = next(self.gen)
            log.info("writing: %s"%data)
            os.write(self.master, data)
            time.sleep(self.interval)


class DequeParser:
    """Stream parser with a circular buffer implemented with a deque."""
    def __init__(self, stream, bufsize=1024):
        log.info("reading from stream: %s"%stream)
        self.stream = stream
        self.buffer = deque([-1], maxlen=bufsize)
        self.lock   = threading.Lock() # maybe not needed if deque is thread-safe?

        log.debug("parser buffer id: %s"%id(self.buffer))

    def run(self):
        log.info("Started stream parser")
        buffer = self.buffer
        while True:
            rlst, wlst, elst = select.select([self.stream], [], [], 0)

            if rlst:
                data = self.stream.read()
                log.info("read: %s"%data)
                val = int.from_bytes(data, 'big')
                log.info("appending: %s"%val)
                buffer.append(val)
            else:
                time.sleep(0.001)


class Actor:
    """Actor which performs tasks using information from a buffer."""
    def __init__(self, buffer, interval=1):
        self.buffer = buffer
        self.interval = interval

        log.debug("actor buffer id: %s"%id(self.buffer))

    def run(self):
        log.info("Started actor.")
        while True:
            current = self.buffer[-1]
            log.info("acting based on value: %s"%current)
            time.sleep(self.interval)


if __name__ == "__main__":
    try:
        # Create & run a dummy stream writer
        ws = SimpleStream(interval=0.01)
        writer = threading.Thread(target=ws.run)
        writer.start()

        # Create & run the stream parser
        ser = serial.Serial(ws.slave_name, timeout=0)
        ser.nonblocking()
        parser = DequeParser(ser, bufsize=1024)
        reader = threading.Thread(target=parser.run)
        reader.start()

        # Create & run the actor
        actor = Actor(parser.buffer, interval=0.1)
        actor.run()
    finally:
        log.info("Cleaning up...")
        # Clean up somehow
        log.info("Done.")
