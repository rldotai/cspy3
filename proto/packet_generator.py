"""
Code for generating fake packets ala the IRobot Create
"""
import logging
import io
import random
import struct
import time

from cspy3 import v1


# Logging, why not
log = logging.getLogger(__name__)
if not log.hasHandlers():
    _handler = logging.StreamHandler()
    _formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-4s %(message)s')
    _handler.setFormatter(_formatter)
    log.addHandler(_handler)
    log.setLevel(logging.DEBUG)


class PacketGenerator:
    """Creates valid packets according to the iRobot Interface spec, with an
    `interval` to simulate a real-time data stream.
    It supports stream-like iteration, raising StopIteration when there are no
    more packets available at the current time.
    """
    FIRST_BYTE = 19

    def __init__(self, *packet_ids, interval=1):
        self.packet_ids = packet_ids
        self.interval = interval
        self.tic = time.time()

        # Perform the rest of the setup
        self.setup()

    def setup(self, packet_ids=None):
        """Set up the packet generator."""
        if packet_ids is None:
            packet_ids = self.packet_ids

        # Get information necessary to build packets
        self.info   = [v1.PACKETS[x] for x in packet_ids]
        self.sizes  = [x['size'] for x in self.info]
        self.names  = [x['name'] for x in self.info]
        self.dtypes = [x['dtype'] for x in self.info]
        self.ranges = [x['ValueRange'] for x in self.info]
        self.count  = len(packet_ids)
        self.format = ">BBB" + "B".join(self.dtypes) + "B"
        self.length = self.count + sum(self.sizes) + 3

        log.debug('names: %s'%self.names)
        log.debug('sizes: %s'%self.sizes)
        log.debug('dtypes: %s'%self.dtypes)
        log.debug('format: %s'%self.format)
        log.debug('number of sensors: %d'%self.count)
        log.debug('packet length: %d'%self.length)

    def make_packet(self):
        """Create a packet."""
        values = [19, self.count]
        for x in self.info:
            values.append(x['id'])
            values.append(random.randint(*x['ValueRange']))
        log.debug("Making packet with values: %s"%values)
        ret = bytearray(struct.pack(self.format[:-1], *values))
        # Checksum
        ret.append(256-sum(ret)%256)
        return ret

    def __iter__(self):
        return self

    def __next__(self):
        now = time.time()
        if now < self.interval + self.tic:
            raise StopIteration
        else:
            self.tic += self.interval
            return self.make_packet()


class ByteStream(io.RawIOBase):
    """Wrapper for iterators that produce bytes."""
    def __init__(self, iterable):
        self.iterator = iter(iterable)
        self.leftover = b''

    def read(self, n=-1):
        """Non-blocking read."""
        if n == -1:
            return self.readall()

        data  = self.leftover
        count = len(data)
        log.debug('count: %d \t data: %s' %(count, data))
        # Read until satisfied or until the generator runs out
        try:
            while count < n:
                chunk = next(self.iterator)
                data += chunk
                count += len(chunk)
                log.debug('count: %d \t data: %s' %(count, data))
                log.debug('chunk: %s'%chunk)

        except StopIteration:
            # log.debug('received stop iteration')
            # log.debug('returning: %s'%data)
            self.leftover = b''
            return data

        self.leftover = data[n:]
        return data[:n]

    def readable(self):
        return True

    def writeable(self):
        return False


class PacketStream(ByteStream):
    """A packet streaming object, employing `PacketGenerator` for generating
    valid packets and subclassing `ByteStream` for the relevant I/O methods.
    """
    def __init__(self, *packet_ids, interval=1):
        self.generator = PacketGenerator(*packet_ids, interval=interval)
        self.iterator = iter(self.generator)
        self.leftover = b''


if __name__ == "__main__":
    pg = PacketGenerator(21, 22, 24)
    st = ByteStream(pg)
    ps = PacketStream(21, 22, 23, 24)