"""
An example script that just shuts down the robot and tells it to stop.
"""
import sys
import cspy3

if __name__ == "__main__":
    ctrl = cspy3.Controller(sys.argv[1])
    print("Stopping robot...")
    ctrl.drive_direct(0, 0)
    ctrl.mode_passive()
    ctrl.pause_stream()
    print("Shutting down controller...")
    ctrl.shutdown()