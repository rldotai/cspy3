"""
An example of how to use cspy3 to request a stream of sensors from the robot,
parse the resulting stream, and output the result in JSON format.
"""
import logging, sys
log = logging.getLogger(__name__)
_log_format = '%(asctime)s %(name)-12s %(levelname)-4s %(message)s'
logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=_log_format)

import argparse
import json
import os
import pty
import random
import select
import struct
import threading
import time
import serial

from collections import deque
from itertools import cycle
from threading import Thread

from cspy3 import Controller, Parser
from cspy3 import v1



class Flag(threading.Event):
    """A wrapper for the typical event class to allow for overriding the
    `__bool__` and `__call__` magic methods, since it looks nicer.
    """
    def __bool__(self):
        return self.is_set()

    def __call__(self):
        return self.is_set()


class StreamReader:
    """Reads streaming data using serial, passes it to a handler."""
    def __init__(self, stream, handler):
        log.debug("Creating StreamReader object for stream: %s"%stream)
        self.stream = stream
        self.handler = handler

    def run(self, halt_condition=None):
        """Begin the stream input processing loop."""
        if halt_condition is None:
            halt_condition = False

        try:
            log.debug("StreamReader started.")
            while not halt_condition:
                rlst, wlst, elst = select.select([self.stream], [], [], 0)

                if rlst:
                    data = self.stream.read()
                    # log.debug("read: %s"%data)
                    self.handler(*data)
                else:
                    time.sleep(0.001)
        finally:
            # Perform cleanup
            log.debug("StreamReader stopped.")



if __name__ == "__main__":
    import sys
    port_name = sys.argv[1]

    # logging.getLogger('cspy3').setLevel(logging.WARN)

    flag = Flag()
    try:
        sensor_ids = [28, 29, 30, 31]

        # initialize the robot
        controller = Controller(port_name)
        controller.mode_full()
        controller.request_stream(*sensor_ids)

        # get serial port for reader
        ser = controller.ser

        # the stream parser and output data store
        parser = Parser(*sensor_ids)
        feed = parser.output

        # stream reader
        reader = StreamReader(ser, parser)

        # Set up threads
        t_reader = Thread(target=reader.run, name="reader", args=(flag,))
        t_reader.start()

        # List of all child threads
        threads = list(threading.enumerate()).remove(threading.main_thread())

        # Main thread for printing
        while not feed:
            log.debug("Waiting for sensor data stream to start...")
            time.sleep(0.1)

        while not flag:
            while not feed:
                time.sleep(0.01)
            obs = feed.pop()
            print(json.dumps(obs), file=sys.stdout, flush=True)




    except KeyboardInterrupt:
        log.debug("Interrupt received, setting quit flag.")
        flag.set()
    finally:
        # Ensure that the flag is set regardless since program is terminating
        log.debug("Starting termination, setting quit flag.")
        flag.set()

        # Join the threads
        log.debug("Attempting to join threads...")
        while threads:
            for t in threads:
                t.join(0.1)
                if t.is_alive():
                    log.debug("Thread %s not ready to join"%t.name)
                else:
                    log.debug("Thread %s successfully joined"%t.name)
                    threads.remove(t)
        log.debug("Program terminated.")