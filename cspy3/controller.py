"""
Code for controlling the iRobot Create.
"""
import logging
import select
import serial
import struct
from time import sleep, time

from . import create_v1 as create
from .utils import clip


# Simple logging
log = logging.getLogger(__name__)


class Controller:
    """A class that wraps commands for the iRobot Create, to handle sending
    commands (but not reading the robot's responses.
    """
    def __init__(self, port_name):
        try:
            self.ser = self._open_port(port_name, create.SERIAL_PARAMS)
            self.mode_passive()
        except Exception as e:
            raise(e)

    def _open_port(self, port_name, serial_params):
        """Open a serial port for connecting to the robot."""
        try:
            ser = serial.Serial(port_name, **serial_params)
            ser.setRTS(0)
            sleep(0.25)
            ser.setRTS(1)
            sleep(0.25)
            ser.flushOutput()
            sleep(0.25)
            return ser
        except Exception as e:
            raise(e)

    def shutdown(self):
        """Shutdown. Pause stream, set mode to passive, and close serial port."""
        try:
            self.pause_stream()
            sleep(1.5)
            self.ser.flushOutput()
            self.mode_passive()
            self.ser.flushOutput()
            sleep(1.5)
        except Exception as e:
            self.ser.flush()
            self.ser.flushOutput()
            self.ser.flushInput()
            self.ser.close()
            raise(e)
        finally:
            self.ser.close()

    def _send_cmd(self, *lst):
        """Send a command (here, a list of integers) to the robot.

        Args:
            lst: a list of int, or other byte-sized objects.

        Returns:
            int: the number of bytes sent
        """
        # TODO: Check endian-ness
        cmd = struct.pack('B'*len(lst), *lst)
        try:
            sent = self.ser.write(cmd)
            self.ser.flushOutput()
        except Exception as e:
            raise(e)
        return sent

    def drive(self, speed, radius):
        """Drive the robot with a given speed and radius."""
        speed = clip(speed, -500, 500)
        radius = clip(radius, -2000, 2000)
        self._send_cmd(create.OP_DRIVE, *bytearray(struct.pack('>2h', speed, radius)))

    def drive_direct(self, left, right):
        """Drive the robot, setting the speed of the left and right wheels."""
        left = clip(left, -500, 500)
        right = clip(right, -500, 500)
        self._send_cmd(create.OP_DRIVE_DIRECT, *bytearray(struct.pack('>2h', left, right)))

    def mode_full(self):
        """Set mode to full."""
        self._send_cmd(create.OP_FULL)

    def mode_passive(self):
        """Set mode to passive."""
        return self._send_cmd(create.OP_START)

    def mode_safe(self):
        """Set mode to safe."""
        self._send_cmd(create.OP_SAFE)

    def pause_stream(self):
        """Pause the stream."""
        return self._send_cmd(create.OP_PAUSE, 0)


    def play_song(self, id_):
        """Plays a previously saved song with the id_ as its id."""
        if id_ not in range(0, 16):
            raise ValueError("id_ not in legal range")
        else:
            return self._send_cmd(create.OP_PLAY, id_)

    def pwm_lsd(ser, pct0=0, pct1=0, pct2=0):
        """ Set up pulse-width modulation on the low side drivers by specifying
        the percentage of maximum power (w/ 7 bit resolution)."""
        # Get the integer representation of the desired power
        d0 = max(0, min(int(128 * pct0), 128))
        d1 = max(0, min(int(128 * pct1), 128))
        d2 = max(0, min(int(128 * pct2), 128))
        # Send the command
        return self._send_cmd(OP_PWM_LSD, d0, d1, d2)

    def request_stream(self, *sensor_ids):
        """Request a stream of the sensors specified by `sensor_ids`."""
        return self._send_cmd(create.OP_STREAM, len(sensor_ids), *sensor_ids)

    def run_demo(self, num):
        """Run a built-in demo.
        num : 0, 1, 2, ..., 9
        """
        ret = self.stop_demo()
        ret += self._send_cmd(create.OP_DEMO, num)
        return ret

    def save_song(self, id_, notes):
        """Saves a sequence of notes under an id between 0 and 15. Each note is
        a tuple where its first element is a note number and its second element
        is a duration.
        """
        if id_ not in range(0, 16):
            raise ValueError("id_ not in legal range")
        else:
            tmp = *list(sum(notes, ())) # flatten the list of note tuples
            return self._send_cmd(create.OP_SONG, id_, len(notes), tmp)

    def set_led(self, playOn=False, advOn=False, powColor=0, powIntensity=0):
        # Set up byte for the Advance and Play LEDs
        tmp = 0
        # do these with binary operators to make them more obvious
        if playOn: tmp |= 2
        if advOn:  tmp |= 8
        # Limit the power LED intensities
        powIntensity = max(0, min(powIntensity, 255))
        powColor     = max(0, min(powColor, 255))

        # send the command
        return self._send_cmd(create.OP_LEDS, tmp, powColor, powIntensity)

    def set_lsd(self, d0=False, d1=False, d2=False):
        """ Set the low side drivers to be on (True) or off (False)
            d0 and d1 (corresponding to pins 22 & 23) have a maximum current
            of 0.5 A, d2 (pin 24) has a maximum current of 1.5 """
        # Build the byte to be sent to the Create
        tmp = 0
        if d0: tmp += 1
        if d1: tmp += 2
        if d2: tmp += 4

        # send the command
        return self._send_cmd(create.OP_LSD, tmp)

    def stop_demo(self):
        return self._send_cmd(create.OP_DEMO, 255)

    def soft_reset(self):
        """Soft reset (an undocumented function).
        Useful when the Create has been switched to Passive Mode WHILE power is
        available  -- it apparently only detects the rising current, so it might
        not actually begin charging.
        This is a somewhat undocumented feature (but is mentioned in the iRobot
        website support FAQ). It forces the robot to run its bootloader.

        IMPORTANT: DO NOT SEND ANY DATA WHILE BOOTLOADER IS RUNNING!
        It takes about three (3) seconds to run the bootloader.
        """
        try:
            ret = self._send_cmd(create.OP_SOFT_RESET)
        except Exception as e:
            print(e)
        finally:
            sleep(3)
        return ret
